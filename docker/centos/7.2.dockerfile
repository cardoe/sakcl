FROM centos:7.2.1511
MAINTAINER Doug Goldstein <cardoe@cardoe.com>

RUN mkdir /source
WORKDIR /source

# setup the PATH for cargo/rustc/etc
RUN mkdir -p /root/.cargo/
ENV PATH "/root/.cargo/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

# pin ourselves to CentOS 7.2
RUN sed \
    -e 's/$releasever/7.2.1511/' \
    -e 's/mirror\./vault\./' \
    -e 's/#baseurl/baseurl/' \
    -e 's/mirrorlist/#mirrorlist/' \
    -i /etc/yum.repos.d/CentOS-Base.repo

# Fix bad rpmdb checksum issues
RUN yum install -y yum-plugin-ovl

# Install build dependencies
RUN yum install -y \
        openssl-devel \
        rpm-build \
        rpm-sign \
        gcc \
        git \
        && \
    yum clean all && \
    rm -rf /var/cache/yum/* /tmp/* /var/tmp/*

RUN curl https://sh.rustup.rs -sSf > rustup-install.sh && \
    sh ./rustup-install.sh -y --default-toolchain 1.42.0-x86_64-unknown-linux-gnu && \
    rm rustup-install.sh

RUN cargo install cargo-rpm --vers 0.7.0
