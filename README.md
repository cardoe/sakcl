[![Build status](https://gitlab.com/cardoe/sakcl/badges/master/pipeline.svg)](https://gitlab.com/cardoe/sakcl/commits/master)
[![Rust version]( https://img.shields.io/badge/rust-1.26+-blue.svg)]()
[![Latest version](https://img.shields.io/crates/v/sakcl.svg)](https://crates.io/crates/sakcl)
[![All downloads](https://img.shields.io/crates/d/sakcl.svg)](https://crates.io/crates/sakcl)
[![Downloads of latest version](https://img.shields.io/crates/dv/sakcl.svg)](https://crates.io/crates/sakcl)

SSH AuthorizedKeysCommand using LDAP (sakcl)

sakcl (pronounced 'sackle' like 'handle') is designed to be called
by OpenSSH as the [AuthorizedKeysCommand](http://man.openbsd.org/cgi-bin/man.cgi/OpenBSD-current/man5/sshd_config.5#AuthorizedKeysCommand).
It is simple to install and simple to configure and works with the other
defaults of OpenSSH.

## Installation

### Cargo

```bash
cargo install sakcl
```

### Binary Packages

```bash
curl -o artifacts.zip https://gitlab.com/cardoe/sakcl/-/jobs/71677129/artifacts/download
unzip artifacts.zip
```

## Configuration

The configuration file by default is located at `/etc/sakcl.conf`. This
can be overwritten by providing the `-c /path/to/config` argument before
the username.

The configuration file must look like:

```toml
uri = "ldaps://ldap.host.name"
base = "ou=Users,dc=company,dc=com"
# basedn and bindpw are optional parameters
basedn = "dn=serviceacct,ou=Users,dc=company,dc=com"
basspw = "12345"
scope = "one|subtree|base"
# filter will have any * replaced with the username supplied
# otherwise it is passed directly to the LDAP search
filter = "(&(objectClass=posixAccount)(uid=*))"
attr = "attribute-with-ssh-public-key"
```

Once this is configured you can test that it works by running:

```bash
sakcl your-ldap-uid
```

And you should see your SSH public key displayed on stdout. To finish
configuring your system to use this change the
[AuthorizedKeysCommand](http://man.openbsd.org/cgi-bin/man.cgi/OpenBSD-current/man5/sshd_config.5#AuthorizedKeysCommand)
to point to your `sakcl` binary and change
[AuthorizedKeysCommandUser](http://man.openbsd.org/cgi-bin/man.cgi/OpenBSD-current/man5/sshd_config.5#AuthorizedKeysCommandUser)
to an unpriviledged account name. Lastly change the ownership of
`/etc/sakcl.conf` to the unpriviledged account name and set the mode to
octal `0400`.
